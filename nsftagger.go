package main

import "fmt"
import "os"
import "bufio"
import "flag"
import "strings"
import "regexp"

var justPrint = flag.Bool("p",false, "Just Print the data")
var template = flag.String("t","-1", "Use Template")
var templateFile = flag.String("tf", "-1", "Template File")
var help = flag.Bool("h",false, "Help!")

type tArg struct{
	exist bool
	index int
	symbol string
	text string
}


func checkForArg(text string,arg tArg,searchString string) tArg{
	tmpIndex := strings.Index(text,searchString)
	if (tmpIndex != -1){
		arg.exist = true
		arg.index = tmpIndex
	}
	return arg
}

func getLowestArgs(args []tArg) (lowest tArg,remaining []tArg){
	lowestNr := 9999
	lowestIndex := 0

	for i:= 0; i < len(args); i++{
		if ((args[i].index != -1) && (args[i].index < lowestNr)){
			lowestNr = args[i].index
			lowestIndex = i
		}
	}

	lowest = args[lowestIndex]
	remaining = append(args[:lowestIndex], args[lowestIndex+1:]...) 
	return
}

func sortArgs(args []tArg) (sortedArgs []tArg){
	var arg tArg
	sortedArgs = make([]tArg,len(args))

	for i:= 0; i<len(sortedArgs); i++{
		arg,args = getLowestArgs(args)
		sortedArgs[i] = arg
	}

	return
}

func parseTemplate(template string) (tArgNum int, tArgs []tArg, tCompRegexp *regexp.Regexp){
	tRegexp := ".*?"
	tArgNum = 0
	tArgs = []tArg{tArg{false,-1,"%A",""}, tArg{false,-1,"%T",""}, tArg{false,-1,"%C",""}}

	//Escape volatile signs.
	modTemplate := strings.Replace(template,".","\\.",-1)
	modTemplate = strings.Replace(modTemplate,"[","\\[",-1)
	modTemplate = strings.Replace(modTemplate,"]","\\]",-1)

	//Add template to regexp search
	tRegexp += modTemplate

	//Find Arguments...
	for i := 0; i < len(tArgs) ; i++ {
		tArgs[i] = checkForArg(modTemplate, tArgs[i], tArgs[i].symbol) 
	}
	
	//...and replace with regexp code
	for i := 0; i < len(tArgs) ; i++ {
		if (tArgs[i].exist){
			tArgNum += 1
			tRegexp = strings.Replace(tRegexp,tArgs[i].symbol,".*?([\\p{Hiragana}\\p{Katakana}\\p{Han}\\x{FF5F}-\\x{FF9F}\\x{3000}-\\x{303F}\\.\\- A-Za-z0-9 \\[\\]+'´~\"\\!üÜ]*)",-1)
		}
	}

	//compile the regexp
	tCompRegexp,errRegexp := regexp.Compile(tRegexp)
	if (errRegexp != nil){ panic("Invalid template: "+tRegexp)}

	//Sort them
	tArgs = sortArgs(tArgs)
	return
}

func readAndReturn(size int,f *os.File) []byte{
	var b = make([]byte, size)
	
	r := bufio.NewReader(f)
	r.Read(b)

	return b
}

func emptyField(size int, f *os.File, offset int64) {
	var b = make([]byte, size, size)
	f.Seek(offset,0)
	w := bufio.NewWriter(f)
	w.Write(b)
	w.Flush()

}

func writeAndReturn(size int,f *os.File) {
	var b = make([]byte, size, size)
	r := bufio.NewReader(os.Stdin)

	w := bufio.NewWriter(f)
	b,_,_ = r.ReadLine()
	if (len(b) < 31){
		_,err := w.Write(b)
		w.Flush()
		if err != nil{
			fmt.Println(err)
		}
	} else {
		_,err := w.Write(b[0:30])
		w.Flush()
		if err != nil{
			fmt.Println(err)
		}
	}
	

}

func writeStringAndReturn(t string,size int,f *os.File) {
	b := []byte(t)

	w := bufio.NewWriter(f)
	if (len(b) < 31){
		_,err := w.Write(b)
		w.Flush()
		if err != nil{
			fmt.Println(err)
		}
	} else {
		_,err := w.Write(b[0:30])
		w.Flush()
		if err != nil{
			fmt.Println(err)
		}
	}
	

}

func getTitle(f *os.File) string{
	f.Seek(0xE,0)
	return string(readAndReturn(32,f))
}

func setTitle(f *os.File){
	emptyField(32, f, 0xE)
	f.Seek(0xE,0)
	writeAndReturn(32,f)
}

func setTitleTo(t string,f *os.File){
	emptyField(32, f, 0xE)
	f.Seek(0xE,0)
	writeStringAndReturn(t,32,f)
}

func setArtist(f *os.File){
	emptyField(32, f, 0x2E)
	f.Seek(0x2E,0)
	writeAndReturn(32,f)
}

func setArtistTo(t string,f *os.File){
	emptyField(32, f, 0x2E)
	f.Seek(0x2E,0)
	writeStringAndReturn(t,32,f)
}
func getArtist(f *os.File) string{
	f.Seek(0x2E,0)
	return string(readAndReturn(32,f))
}

func tag(fileName string){
	wd,_ :=os.Getwd()
	infile,_ := os.OpenFile(wd+"/"+fileName, os.O_RDWR,0777)

	fmt.Printf("\n\nFileName: %v\nTitle: %v\nArtist: %v\n",fileName,getTitle(infile),getArtist(infile))
	if !*justPrint{
		fmt.Printf("Set Title: ")
		setTitle(infile)
		fmt.Printf("Set Artist: ")
		setArtist(infile)
	}
	infile.Close()
}

func tagFromFile(r *bufio.Reader,t string,fileName string){
	var artist string
	var title string
	var album string

	wd,_ :=os.Getwd()
	infile,_ := os.OpenFile(wd+"/"+fileName, os.O_RDWR,0777)

	artist = getArtist(infile)
	title = getTitle(infile)
	album = ""
	album = album

	tempLine,_,_ := r.ReadLine()
	textParsed := false

	fmt.Printf("---\nBefore:\n\nFileName: %v\nTitle: %v\nArtist: %v\n",fileName,title,artist)

	if !*justPrint{
		argNum,tArgs,tRegexp := parseTemplate(t)

		search := tRegexp.FindAllStringSubmatch(string(tempLine),1)
		searchResultNum := len(search[0])-1

		if (argNum == searchResultNum) {
			for i:=0;i<searchResultNum;i++{
				tArgs[i].text = search[0][i+1]
			}
			textParsed = true
		} else {
			fmt.Println("Error parsing file, skipping.")
		}
		if (textParsed){
			for i:=0;i<len(tArgs);i++{
				if ((tArgs[i].symbol == "%A")&&(tArgs[i].text != "")) {
					artist = tArgs[i].text
				} else if ((tArgs[i].symbol == "%T")&&(tArgs[i].text != "")) {
					title = tArgs[i].text
				} else if ((tArgs[i].symbol == "%C")&&(tArgs[i].text != "")) {
					album = tArgs[i].text
				}
			}
			if (artist != "") {setArtistTo(artist,infile)}
			if (title != "") {setTitleTo(title,infile)}
		}
	}
	infile.Close()
	fmt.Printf("---\nAfter:\n\nFileName: %v\nTitle: %v\nArtist: %v\n",fileName,title,artist)
}

func loadTemplateFile(fileName string) *bufio.Reader{
	wd,_ :=os.Getwd()
	infile,_ := os.OpenFile(wd+"/"+fileName, os.O_RDWR,0777)
	tFileReader := bufio.NewReader(infile)

	return tFileReader
}
func main() {
	flag.Parse()
	fmt.Println("NSF Tagger v0.2 - By Magnus Aspling")	
	if *help{
		fmt.Println("\nUsage: nsftagger [options] [target] ...\nOptions: -p Print run (No tagging)\n-t Template (Use with -tf)\n-tf TemplateFile\n-h Help\n\nBug reports at https://bitbucket.org/yugge/nsftagger/")
	} else { 
		tFileReader := loadTemplateFile(*templateFile)
		for i:=0; i < flag.NArg(); i++ {
			if ((*template != "-1") && (*templateFile != "-1")){
				tagFromFile(tFileReader,*template,flag.Arg(i)) 
			} else {
				tag(flag.Arg(i))
			}
		}
	}
}
